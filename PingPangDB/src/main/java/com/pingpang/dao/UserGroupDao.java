package com.pingpang.dao;

import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.pingpang.websocketchat.ChatGroup;


/**
 * 用户群组建立
* @author dell
*/
@Mapper
public interface UserGroupDao {
	/**
	 * 获取所有群组信息
	 * @param queryMap
	 * @return
	 */
    public Set<ChatGroup> getAllGroupSet(Map<String,String> queryMap);
    
    /**
	  * 获取群组总数
	 * @param queryMap
	 * @return
	 */
	public int getAllGroupCount(Map<String,String> queryMap);
	
	/**
	 * 添加群组
	 * @param cg
	 */
	public void addGroup(ChatGroup cg);
	
	/**
	 * 跟新群组信息
	 * @param cg
	 */
	public void updateGroup(ChatGroup cg);
	
	/**
	   *  更新数据
	   *  状态0正常  -1禁用
	 * @param id
	 */
	public void updateGroups(@Param("status")String status,@Param("ids")Set<String>ids);
	
	/**
	 * 获取群组信息
	 * @param queryMap
	 * @return
	 */
	public ChatGroup getGroup(Map<String,String> queryMap);
	
	
	/**
	 * 获取群组信息
	 * @param ids
	 * @return
	 */
	public Set<ChatGroup> getGroupByID(@Param("ids")Set<String>ids);
	
}
