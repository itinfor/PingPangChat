package com.pingpang.redis.init;

import java.util.HashMap;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pingpang.redis.RedisPre;
import com.pingpang.redis.service.RedisService;
import com.pingpang.service.UserService;
import com.pingpang.websocketchat.ChartUser;
 
@Component
public class RedisInit  implements InitializingBean {

	private Logger logger = LoggerFactory.getLogger(RedisInit.class);
	 
	 @Autowired
	 private UserService userService;
	
	 @Autowired
	 private RedisService redisService;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		/*
         logger.info("----------数据删除开始--------------------------------------------");		  
         //redisService.deletePrex(RedisPre.DB_USER+"*");	
         //redisService.deletePrex(RedisPre.DB_GROUP_SET+"*");
         logger.info("----------数据删除结束--------------------------------------------");
         
         logger.info("----------缓存用户初始化开始----------------------------------");
	     Set<ChartUser> userList =userService.getRedisAllUser(new HashMap<String, String>());
	     if(null!=userList) {
	    	 for(ChartUser cu: userList) {
	    		 redisService.set(RedisPre.DB_USER+cu.getUserCode(), cu);
	    	 }
	     }
	     logger.info("----------缓存用户初始化结束----------------------------------");
	     */
	}
}