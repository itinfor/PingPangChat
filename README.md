# PingPangChat

#### 介绍

基于netty的websocket即时聊天程序

功能：单聊、群聊、语音录制推送、视频聊天、直播、历史聊天对象

#### 演示

https://pingpangchat.xyz/

前端:https,服务端:wss

#### 软件架构

聊天前台：layui

后台管理：X-admin

后端框架：springboot2,netty,druid,mybatis,redis,zookeeper

         服务端支持集群部署

#### 功能界面

![输入图片说明](https://images.gitee.com/uploads/images/2021/0109/225440_53eec829_62082.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0119/224424_49d4bf25_62082.png "屏幕截图.png")

#### 功能点

```
1.单聊
2.群聊
3.图片、表情、音频、视频发送
4.聊天记录（目前是5条）
5.支持录音发送音频
6.支持视频聊天
7.支持直播（目前是根据视频来的）、也可以搭建flv拉流（需要自己搭环境）
8.断线重连
9.后台管理在线用户、注册用户、群组、消息管理
10.管理员广播
11.实验性功能人脸检测（借助的opencv）
```


#### 数据传输格式为自定义JSON格式

信息主体格式
```
{
    "cmd":null,
    "from":null,
    "accept":null,
    "group":null,
    "msg":null,
    "status":null,
    "id":null,
    "createDate":"2021-01-09 22:26:14",
    "chatSet":null,
    "groupSet":null,
    "oldMsg":null
}
```

绑定服务端(客户端验证成功后获取token绑定)

```
{
    "cmd":"1",
    "from":{
        "userCode":"dnmt",
        "userName":"大内密探"
    },
    "msg":"39cb8c2a98914f0a8a84c09a34990af8"
}
```

单聊

```
{
    "cmd":"3",
    "from":{
        "userCode":"dnmt"
    },
    "accept":{
        "userCode":"fhx"
    },
    "msg":"单聊测试"
}
```

群聊

```
{
    "cmd":"4",
    "from":{
        "userCode":"dnmt"
    },
    "group":{
        "groupCode":"g003"
    },
    "msg":"群聊测试"
}
```

#### 程序使用

1.初始化db脚本

2.修改配置文件application.properties中的db、redis、zookeeper信息即可

3.视频聊天和直播部分用的是peerjs
  
  需要自己配置node的服务端


你的支持是我的动力

![输入图片说明](https://images.gitee.com/uploads/images/2020/0726/101756_8e1520b9_62082.png "屏幕截图.png")