package com.pingpang.image;

import java.awt.AlphaComposite;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.dnn.Dnn;
import org.opencv.dnn.Net;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;


@Component
public class ImageUtil implements CommandLineRunner {
	private static Logger logger = LoggerFactory.getLogger(ImageUtil.class);

	@Value("${opencv.file.path}")
	private String PATH;
			
	@Value("${opencv.save.path}")
	private String SCAN_PATH;
	
	//人脸检测
	private static final String FACEPATH="haarcascade_frontalface_alt2.xml";
	private static final String PROFILEFACEPATH="haarcascade_profileface.xml";
	private static CascadeClassifier FACEBOOK;
	private static CascadeClassifier PROFILEFACEFACEBOOK;
	
	//年龄识别
	private static final String AGE_MODEL = "age_net.caffemodel";
	private static final String AGE_TEXT = "deploy_age2.prototxt";
	private static final String[] AGES = new String[]{"0-2", "4-6", "8-13", "15-20", "25-32", "38-43", "48-53", "60+"};
	private static Net AGENET;
	
	//性别识别
	private static final String GENDER_MODEL = "gender_net.caffemodel";
	private static final String GENDER_TEXT = "deploy_gender2.prototxt";
	private static final String[] GENDERS = new String[]{"男", "女"};;
	private static Net GENDER;
	
	
//	static {
//		try {
//			//PATH=ResourceUtils.getFile("classpath:opencv").getAbsolutePath()+File.separator;
//			//ResourceUtils.
//			InputStream inputStream = ImageUtil.class.getResourceAsStream("/opencv/haarcascade_frontalface_alt2.xml");
//			File docxFile = new File(FACEPATH);
//	        // 使用common-io的工具类即可转换
//	        FileUtils.copyInputStreamToFile(inputStream,docxFile);
//	        inputStream.close();
//	        
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
	
	//文件后缀
	private static final List<String> FILETYPELIST = new ArrayList<String>(Arrays.asList( 
            ".jpg", ".bmp", ".jpeg", ".png", ".gif",
            ".JPG", ".BMP", ".JPEG", ".PNG", ".GIF"));
	
	//目标检测
	private static Map<String,List<Mat>> srcMap=new HashMap<String,List<Mat>>();
	
	/**
	 * 路径图片转为base64
	 * 
	 * @param imagePath
	 * @return
	 * @throws IOException
	 */
	public static Mat imagePath2Mat(String imagePath) throws IOException {
		// 注释部分模拟传入base64数据
		BufferedImage image = ImageIO.read(new FileInputStream(imagePath));
		Mat matImage = ImageUtil.BufImg2Mat(image, BufferedImage.TYPE_3BYTE_BGR, CvType.CV_8UC3);// CvType.CV_8UC3
		// 小型图片可以输出 查看下
//		logger.info(matImage.dump());
//		mat = matImage.dump();
		return matImage;
	}
	/**
	 * base64转Mat
	 * 
	 * @param base64
	 * @return
	 * @throws Exception 
	 */
	public static Mat base642Mat(String base64) throws Exception {
		// 对base64进行解码
		//BASE64Decoder decoder = new BASE64Decoder();
		//byte[] origin = decoder.decodeBuffer(base64);
		byte[] origin=decryptBASE64(base64);
		InputStream in = new ByteArrayInputStream(origin); // 将b作为输入流；
		BufferedImage image = ImageIO.read(in);
		Mat matImage = ImageUtil.BufImg2Mat(image, BufferedImage.TYPE_3BYTE_BGR, CvType.CV_8UC3);// CvType.CV_8UC3
		return matImage;
	}
	/**
	 * 
	 * @param base64
	 * @throws Exception 
	 */
	public static Rect base642Rect(String base64) throws Exception {
		//BASE64Decoder decoder = new BASE64Decoder();
		//byte[] origin = decoder.decodeBuffer(base64);
		byte[] origin=decryptBASE64(base64);
		InputStream in = new ByteArrayInputStream(origin); // 将b作为输入流；
		BufferedImage image = ImageIO.read(in);
		return new Rect(0,0,image.getWidth(),image.getHeight());
	}
	
	/**
	 * 
	 * @param base64
	 * @throws IOException 
	 */
	public static Rect BufferedImage2Rect(BufferedImage image) throws IOException {
		return new Rect(0,0,image.getWidth(),image.getHeight());
	}
	/**
	 * 
	 * @param base64
	 * @throws Exception 
	 */
	public static BufferedImage base642BufferedImage(String base64) throws Exception {
		//BASE64Decoder decoder = new BASE64Decoder();
		//byte[] origin = decoder.decodeBuffer(base64);
		byte[] origin=decryptBASE64(base64);
		InputStream in = new ByteArrayInputStream(origin); // 将b作为输入流；
		return ImageIO.read(in);
	}

	
    /**
     * BASE64Encoder 加密
     * 
     * @param data
     *            要加密的数据
     * @return 加密后的字符串
     */
    public static String encryptBASE64(byte[] data) {
        // BASE64Encoder encoder = new BASE64Encoder();
        // String encode = encoder.encode(data);
        // 从JKD 9开始rt.jar包已废除，从JDK 1.8开始使用java.util.Base64.Encoder
        String encode = Base64.getMimeEncoder().encodeToString(data);
        return encode;
    }
    /**
     * BASE64Decoder 解密
     * 
     * @param data
     *            要解密的字符串
     * @return 解密后的byte[]
     * @throws Exception
     */
    public static byte[] decryptBASE64(String data) throws Exception {
        // BASE64Decoder decoder = new BASE64Decoder();
        // byte[] buffer = decoder.decodeBuffer(data);
        // 从JKD 9开始rt.jar包已废除，从JDK 1.8开始使用java.util.Base64.Decoder
    	if(null!=data && data.indexOf(",")>-1) {
    		data=data.substring(data.indexOf(","));
    	}
        byte[] buffer = Base64.getMimeDecoder().decode(data);
        return buffer;
    }

	
	/**
	 * BufferedImage转换成Mat
	 * 
	 * @param original 要转换的BufferedImage
	 * @param imgType  bufferedImage的类型 如 BufferedImage.TYPE_3BYTE_BGR
	 * @param matType  转换成mat的type 如 CvType.CV_8UC3
	 */
	public static Mat BufImg2Mat(BufferedImage original, int imgType, int matType) {
		if (original == null) {
			throw new IllegalArgumentException("original == null");
		}
		//System.loadLibrary("opencv_java412");
		//System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		//System.load("E:\\opencv\\opencv\\build\\java\\x64\\opencv_java412.dll");
		//logger.info(Core.NATIVE_LIBRARY_NAME);
		// Don't convert if it already has correct type
		if (original.getType() != imgType){
			// Create a buffered image
			BufferedImage image = new BufferedImage(original.getWidth(), original.getHeight(), imgType);

			// Draw the image onto the new buffer
			Graphics2D g = image.createGraphics();
			try {
				g.setComposite(AlphaComposite.Src);
				g.drawImage(original, 0, 0, null);
			} finally {
				g.dispose();
			}
		}

		byte[] pixels = ((DataBufferByte) original.getRaster().getDataBuffer()).getData();
		Mat mat = Mat.eye(original.getHeight(), original.getWidth(), matType);
		mat.put(0, 0, pixels);
		return mat;
	}

	/**
	 * Mat转换成BufferedImage
	 * 
	 * @param matrix        要转换的Mat
	 * @param fileExtension 格式为 ".jpg", ".png", etc
	 * @return
	 */
	public static BufferedImage Mat2BufImg(Mat matrix, String fileExtension) {
		// convert the matrix into a matrix of bytes appropriate for
		// this file extension
		MatOfByte mob = new MatOfByte();
		Imgcodecs.imencode(fileExtension, matrix, mob);
		// convert the "matrix of bytes" into a byte array
		byte[] byteArray = mob.toArray();
		BufferedImage bufImage = null;
		try {
			InputStream in = new ByteArrayInputStream(byteArray);
			bufImage = ImageIO.read(in);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bufImage;
	}

	/**
	 * Mat转换成BufferedImage
	 * 
	 * @param matrix        要转换的Mat
	 * @param fileExtension 格式为 ".jpg", ".png", etc
	 * @return
	 */
	public static String Mat2Base64(Mat matrix, String fileExtension) {
		// convert the matrix into a matrix of bytes appropriate for
		// this file extension
		MatOfByte mob = new MatOfByte();
		Imgcodecs.imencode(fileExtension, matrix, mob);
		// convert the "matrix of bytes" into a byte array
		byte[] byteArray = mob.toArray();
		BufferedImage bufImage = null;
		String base64="";
		try {
			InputStream in = new ByteArrayInputStream(byteArray);
			bufImage = ImageIO.read(in);
			//输出流
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			ImageIO.write(bufImage, "png", stream);
			base64 = encryptBASE64(stream.toByteArray());
			//logger.info(base64);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return base64;
	}
	
	/**
	 * Mat转换保存成Image
	 * 
	 * @param matrix        要转换的Mat
	 * @param fileExtension 格式为 ".jpg", ".png", etc
	 * 
	 * @return
	 */
	public static void Mat2Img(Mat matrix, String fileExtension, String pathAndName) {
		// convert the matrix into a matrix of bytes appropriate for
		// this file extension
		MatOfByte mob = new MatOfByte();
		Imgcodecs.imencode(fileExtension, matrix, mob);
		// convert the "matrix of bytes" into a byte array
		byte[] byteArray = mob.toArray();

		BufferedImage bufImage = null;
		try {
			InputStream in = new ByteArrayInputStream(byteArray);
			bufImage = ImageIO.read(in);
			writeImageFile(bufImage, pathAndName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 将bufferdimage转换为图片
	 * 
	 * @param bi
	 * @param pathAndName
	 * @throws IOException
	 */
	public static void writeImageFile(BufferedImage bi, String pathAndName) throws IOException {
		File outputfile = new File(pathAndName);
		ImageIO.write(bi, "jpg", outputfile);
	}

	/**
	 *Scalar(255, 0, 0)  ----表示纯蓝色
     *Scalar(0, 255, 0) ----表示纯绿色
     *Scalar(0, 0, 255) ----表示纯红色
     *Scalar(255, 255, 0) ----表示青色
     *Scalar(0, 255, 255) ----表示黄色
	 * @param image
	 * @return
	 * @throws IOException 
	 */
	public static Mat getFace(Mat image) throws IOException {
		
		//正脸蓝色
		Map<String,Object> resultMap=ImageUtil.getFace(FACEBOOK, image,new Scalar(0,255,255),false);
		if(!(boolean)resultMap.get("check")) {
			//右侧脸青色
			resultMap=ImageUtil.getFace(PROFILEFACEFACEBOOK, image,new Scalar(255, 255, 0),false);
			if(!(boolean)resultMap.get("check")) {
				//左侧脸蓝色
				resultMap=ImageUtil.getFace(PROFILEFACEFACEBOOK, image,new Scalar(255, 0, 0),true);
			}
		}
		
		if((boolean) resultMap.get("check")) {
			//ImageUtil.Mat2Img(image, ".jpeg", "E:\\opencv4.4.0\\age-and-gender-classification\\img\\test_me\\" + UUID.randomUUID()+".jpeg");		
		}
		
		return (Mat) resultMap.get("Mat");
	}

	/**
	 * 
	 * @param cc 识别类
	 * @param image 图片
	 * @param sc 颜色
	 * @param flip 是否反转
	 * @return
	 * @throws IOException 
	 */
	private static Map<String,Object> getFace(CascadeClassifier cc,Mat image,Scalar sc,boolean flip) throws IOException {
        Map<String,Object> resultMap=new HashMap<String,Object>();
        
        Mat imageCloneCheck=image.clone();
        Mat imageClone=new Mat();
        Imgproc.cvtColor(image, imageClone, Imgproc.COLOR_BGR2GRAY);
        
		MatOfRect face = new MatOfRect();
		
		if(flip) {
			Core.flip(imageClone, imageClone, 1);
			Core.flip(imageCloneCheck, imageCloneCheck, 1);
		}
		cc.detectMultiScale(imageClone, face);
		
		Rect[] rects = face.toArray();
		logger.info("匹配到 " + rects.length + " 个人脸");
		
		String name="";
		Mat checkMat=null;
		if (rects.length > 0) {
			Map<String,Object> isCheck = ImageUtil.compareHist(image);
			if ("1".equals(isCheck.get("statu"))) {
				name=(String) isCheck.get("name");
				checkMat=(Mat) isCheck.get("image");
				logger.info("-----------------------");
				logger.info("----------"+name+"-------------");
				logger.info("-----------------------");
			}
		}
		
		// 4 为每张识别到的人脸画一个圈
		for (int i = 0; i < rects.length; i++) {
			
			//Mat areaM = new Mat(image, rects[i]);
			//ImageUtil.Mat2Img(areaM, ".jpeg", "E:\\opencv4.4.0\\age-and-gender-classification\\img\\huge\\"+UUID.randomUUID()+".jpeg");
			
			Imgproc.rectangle(imageCloneCheck, new Point(rects[i].x, rects[i].y),
					new Point(rects[i].x + rects[i].width, rects[i].y + rects[i].height), sc);
			
			String age=analyseAge(imageCloneCheck,rects[i]);
			String gender=analyseGender(imageCloneCheck,rects[i]);
		    
			if(flip) {
				Core.flip(imageCloneCheck, imageCloneCheck, 1);
			}	
			
			//
			Font font = new Font("微软雅黑", Font.PLAIN, 12); 
			BufferedImage bufImg =Mat2BufImg(imageCloneCheck,".png");
			Graphics2D g = bufImg.createGraphics();
            g.drawImage(bufImg, 0, 0, bufImg.getWidth(),bufImg.getHeight(), null);
            g.setFont(font);              //设置字体
            
            //设置水印的坐标
            g.drawString("性别:"+gender+" 年龄:"+age+" 名称:"+(!"".equals(name)?name:" 未识别"), rects[i].x, rects[i].y);
	        g.dispose();
	        
	        imageCloneCheck=ImageUtil.BufImg2Mat(bufImg, BufferedImage.TYPE_3BYTE_BGR, CvType.CV_8UC3);// CvType.CV_8UC3
            
			/*
			 * Imgproc.putText(image, new String(("性别:" + gender + "年龄:" +
			 * age).getBytes("UTF-8")), new Point(rects[i].x, rects[i].y),
			 * Imgproc.FONT_HERSHEY_PLAIN, 0.8, sc, 1, Imgproc.LINE_AA, false);
			 */
			 
		}
		
		/*
		if(flip) {
			Core.flip(imageCloneCheck, imageCloneCheck, 1);
		}*/
		
		  if(null!=checkMat) {
			    Mat dstImage = Mat.ones(60,60,CvType.CV_8UC3);
		        // 会根据输出图像 dstImage的大小，自动计算出 fx，fy，进行缩放
		        Imgproc.resize(checkMat,dstImage,dstImage.size());
		        Rect rect = new Rect(5,5, dstImage.cols(),dstImage.rows());//x,y坐标，宽，高
		        Mat src_roi = new Mat(imageCloneCheck, rect);
		        //Core.addWeighted(src_roi, 0.5, dstImage, 0.5, 0, src_roi);
		        dstImage.copyTo(src_roi);
		  }
		 
		boolean check=rects.length<1?false:true;
		resultMap.put("check", check);
		resultMap.put("Mat", imageCloneCheck);
		return resultMap;
	}
	
	
	private static String analyseAge(Mat mRgba, Rect face) {
        try {
            Mat capturedFace = new Mat(mRgba, face);
            //Resizing pictures to resolution of Caffe model
            Imgproc.resize(capturedFace, capturedFace, new Size(227, 227));
            //Converting RGBA to BGR
            Imgproc.cvtColor(capturedFace, capturedFace, Imgproc.COLOR_RGBA2BGR);

            //Forwarding picture through Dnn
            Mat inputBlob = Dnn.blobFromImage(capturedFace, 1.0f, new Size(227, 227),
                    new Scalar(78.4263377603, 87.7689143744, 114.895847746), false, false);
            AGENET.setInput(inputBlob, "data");
            Mat probs = AGENET.forward("prob").reshape(1, 1);
            Core.MinMaxLocResult mm = Core.minMaxLoc(probs); //Getting largest softmax output

            double result = mm.maxLoc.x; //Result of age recognition prediction
            return AGES[(int) result];
        } catch (Exception e) {
           e.printStackTrace();
        }
        return "";
    }
	
	private static String analyseGender(Mat mRgba, Rect face) {
        try {
            Mat capturedFace = new Mat(mRgba, face);
            //Resizing pictures to resolution of Caffe model
            Imgproc.resize(capturedFace, capturedFace, new Size(227, 227));
            //Converting RGBA to BGR
            Imgproc.cvtColor(capturedFace, capturedFace, Imgproc.COLOR_RGBA2BGR);

            //Forwarding picture through Dnn
            Mat inputBlob = Dnn.blobFromImage(capturedFace, 1.0f, new Size(227, 227),
                    new Scalar(78.4263377603, 87.7689143744, 114.895847746), false, false);
            GENDER.setInput(inputBlob, "data");
            Mat probs = GENDER.forward("prob").reshape(1, 1);
            Core.MinMaxLocResult mm = Core.minMaxLoc(probs); //Getting largest softmax output

            double result = mm.maxLoc.x; //Result of gender recognition prediction. 1 = FEMALE, 0 = MALE
            return GENDERS[(int) result];
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return "";
    }
	
	
	
	public static void initComPare(String path) throws IOException {
		    //srcList.clear();
			File imgFile=new File(path);
			File listFile[]=imgFile.listFiles(new FilenameFilter() {
				//想要保存的文件则，return true;反之return false
				@Override
				public boolean accept(File dir, String name) {
					if(new File(dir,name).isDirectory()) {
						try {
							initComPare(dir.getAbsoluteFile()+File.separator+name);
							return false;
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					String nameType=name.substring(name.lastIndexOf("."));
					if(FILETYPELIST.contains(nameType)){
						return true;
					}
					return false;
				}
			});
			
			
			if(null==listFile) {
				logger.info("获取匹配文件{}:{}个：",imgFile.getName(),0);
				return;
			}
			
			 logger.info("获取匹配文件{}:{}个：",imgFile.getName(),listFile.length);
			 
			 if(listFile.length>0) {
				 //List list=srcMap.get(imgFile.getName());
				 //if(null==list) {
				     srcMap.remove(imgFile.getName());
					 srcMap.put(imgFile.getName(), new ArrayList<Mat>());
				 //}
			 }
			 
			 for(File f:listFile) {
		    	 Mat srcOld=ImageUtil.imagePath2Mat(f.getPath());
		    	 srcMap.get(imgFile.getName()).addAll(getCompareFace(srcOld));
			 }
	}
	
	/**
	 * 通过直方图比较两张图片
	 * @param _src   原始图路径
	 * @param _des   目标图
	 * @return
	 * @throws IOException 
	 */
	@SuppressWarnings("unused")
	private static Map<String,Object> compareHist(Mat _des) throws IOException {
		 Map<String,Object> resultMap=new HashMap<String,Object>();
	     List<Mat> desList=getCompareFace(_des);
	     logger.info("检测人脸："+desList.size());
	     if(desList.size()<1) {
	    	 resultMap.put("statu", "0");
	    	 return resultMap;
	     }
	     Set<String> nameSet=srcMap.keySet();
	    	 for(Mat desM:desList) {
	    	   for(String key:nameSet) {
	    		 for(Mat srcM:srcMap.get(key)) {
	    			 int check=compareHist(srcM,desM);
	    			 if(check>=1) {
	    				 resultMap.put("statu", "1");
	    				 resultMap.put("name", key);
	    				 resultMap.put("image", srcM);
	    				 return resultMap;
	    			 }	    		 
	    		  }
	    		}
	    	 }
	    logger.info("匹配失败");
	    resultMap.put("statu", "0");
   	    return resultMap;
	}
	
	/**
	 * 通过直方图比较两张图片
	 * @param _src  原始图
	 * @param _des   目标图
	 * @return
	 */
	
	private static int i=1;
	private static int compareHist(Mat _src, Mat _des) {
		try {
          /*
			Mat mat_src = _src;
			Mat mat_des = _des;

			if (mat_src.empty() || mat_des.empty()) {
				throw new Exception("no file.");
			}

			Mat hsv_src = new Mat();
			Mat hsv_des = new Mat();

			// 转换成HSV
			Imgproc.cvtColor(mat_src, hsv_src, Imgproc.COLOR_BGR2HSV);
			Imgproc.cvtColor(mat_des, hsv_des, Imgproc.COLOR_BGR2HSV);

			List<Mat> listImg1 = new ArrayList<Mat>();
			List<Mat> listImg2 = new ArrayList<Mat>();
			listImg1.add(hsv_src);
			listImg2.add(hsv_des);

			MatOfFloat ranges = new MatOfFloat(0, 255);
			MatOfInt histSize = new MatOfInt(50);
			MatOfInt channels = new MatOfInt(0);

			Mat histImg1 = new Mat();
			Mat histImg2 = new Mat();

			Imgproc.calcHist(listImg1, channels, new Mat(), histImg1, histSize,
					ranges);
			Imgproc.calcHist(listImg2, channels, new Mat(), histImg2, histSize,
					ranges);

			Core.normalize(histImg1, histImg1, 0, 1, Core.NORM_MINMAX, -1,
					new Mat());
			Core.normalize(histImg2, histImg2, 0, 1, Core.NORM_MINMAX, -1,
					new Mat());
            */
			
			Mat hvs_1 = new Mat();
			Mat hvs_2 = new Mat();
			//图片转HSV COLOR_BGR2HSV COLOR_RGB2GRAY COLOR_BGR2YCrCb
			Imgproc.cvtColor(_src, hvs_1,Imgproc.COLOR_RGB2GRAY);
			Imgproc.cvtColor(_des, hvs_2,Imgproc.COLOR_RGB2GRAY);

			//Imgproc.equalizeHist(hvs_1,hvs_1);
			//Imgproc.equalizeHist(hvs_2,hvs_2);
			
			//ImageUtil.Mat2Img(hvs_1, ".jpeg", "E:\\opencv4.4.0\\age-and-gender-classification\\img\\huge\\compare\\"+i+"_"+ UUID.randomUUID()+".jpeg");
			//ImageUtil.Mat2Img(hvs_2, ".jpeg", "E:\\opencv4.4.0\\age-and-gender-classification\\img\\huge\\compare\\"+i+"_"+ UUID.randomUUID()+".jpeg");
			//i++;
			
			Mat hist_1 = new Mat();
			Mat hist_2 = new Mat();

			//直方图计算
			Imgproc.calcHist(Stream.of(hvs_1).collect(Collectors.toList()),new MatOfInt(0),new Mat(),hist_1,new MatOfInt(100) ,new MatOfFloat(0,256));
			Imgproc.calcHist(Stream.of(hvs_2).collect(Collectors.toList()),new MatOfInt(0),new Mat(),hist_2,new MatOfInt(100) ,new MatOfFloat(0,256));

			//图片归一化
			Core.normalize(hist_1, hist_1, 1, hist_1.rows() , Core.NORM_MINMAX, -1, new Mat() ); 
			Core.normalize(hist_2, hist_2, 1, hist_2.rows() , Core.NORM_MINMAX, -1, new Mat() ); 
			
			double result0, result1, result2, result3;
			result0 = Imgproc.compareHist(hist_1, hist_2, 0);
			result1 = Imgproc.compareHist(hist_1, hist_2, 1);
			result2 = Imgproc.compareHist(hist_1, hist_2, 2);
			result3 = Imgproc.compareHist(hist_1, hist_2, 3);

			// 0 - 相关性：度量越高，匹配越准确 “> 0.9”
			// 1 - 卡方: 度量越低，匹配越准确 "< 0.1"
			// 2 - 交叉核: 度量越高，匹配越准确 "> 1.5"
			// 3 - 巴氏距离: 度量越低，匹配越准确 "< 0.3"
			//logger.info("相关性（度量越高，匹配越准确 [基准：0.9]）,当前值:" + result0);
			//logger.info("卡方（度量越低，匹配越准确 [基准：0.1]）,当前值:" + result1);
			//logger.info("交叉核（度量越高，匹配越准确 [基准：1.5]）,当前值:" + result2);
			//logger.info("巴氏距离（度量越低，匹配越准确 [基准：0.3]）,当前值:" + result3);

			int count = 0;
			if (result0 > 0.9)
				count++;
			if (result1 < 0.1)
				count++;
			if (result2 > 1.5)
				count++;
			if (result3 < 0.3)
				count++;
			int retVal = 0;
			if (count >= 3) {
				//这是相似的图像
				logger.info("测试成功");
				retVal = 1;
			}

			return retVal;
		} catch (Exception e) {
			logger.info("例外:" + e);
		}
		return 0;
	}
	
	
	
	/**
	 * 
	 * @param cc 识别类
	 * @param image 图片
	 * @param sc 颜色
	 * @param flip 是否反转
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	private static List<Mat> getCompareFace(Mat image) throws UnsupportedEncodingException {
        
		List<Rect> rectList=new ArrayList<Rect>();
		Mat sorce=image.clone();
		
		MatOfRect face = new MatOfRect();
		FACEBOOK.detectMultiScale(sorce, face);
		rectList.addAll(face.toList());
		
		face = new MatOfRect();
		PROFILEFACEFACEBOOK.detectMultiScale(sorce, face);
		rectList.addAll(face.toList());
		
	    Core.flip(sorce, sorce, 1);
	    
	    face = new MatOfRect();
		PROFILEFACEFACEBOOK.detectMultiScale(sorce, face);
		rectList.addAll(face.toList());
		
		//ImageUtil.Mat2Img(image, ".jpeg", "E:\\opencv4.4.0\\age-and-gender-classification\\img\\huge\\test.jpeg");
		List<Mat>matList=new ArrayList<Mat>();
		for(Rect rt : rectList) {
			matList.add(new Mat(image, rt));
		}
		return matList;
	}
	
	public static void main(String[] args) throws Exception {
		//new ImageUtil().run("");
		
		/*
		 * String msg=
		 * "\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDABsSFBcUERsXFhceHBsgKEIrKCUlKFE6PTBCYFVlZF9VXVtqeJmBanGQc1tdhbWGkJ6jq62rZ4C8ybqmx5moq6T/2wBDARweHigjKE4rK06kbl1upKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKT/wAARCAEsASwDASIAAhEBAxEB/8QAGgAAAgMBAQAAAAAAAAAAAAAAAQIAAwQFBv/EADQQAAICAQMCBQIEBQQDAAAAAAABAgMRBCExEkEFEyJRYXGBIzJSkRQzQrHBYnKh0STw8f/EABcBAQEBAQAAAAAAAAAAAAAAAAABAgP/xAAcEQEBAQEBAQEBAQAAAAAAAAAAARECMSFBA1H/2gAMAwEAAhEDEQA/ANQSIgQSACBAoAQCQAQIEBACggQQIQhAokIQCEIQCEIACACAAMVsLFYQGxGMxWArEbGYkgEkVTLJFc+ArLdGVmFFbZ3+EGv8rRJNpEre+CoqsWGWy9VafwJah6nmvHsBKX1V49gVPptx7kp2m4gt9M1JdgLtRHqhkyRk4pr5Ny9dZgtWJtYBHoyAJkgKZABAgQBAJABAJABAJABCoEhAIEgACQz6nVV6eHVOWPZGKzxWGPTu2B0pWRjy0LK2MYOWeDz12tsnJ+pjw1tii03lNAd2Fql32HyedhqZxisNrf3OtodVGyGJP1IDWxWNs+AMCtisdisBGJIsZXICpiSLGVWvC2Aol3FjtIZZcdxXsVBtWwtDxJx+CyW8clMX02JhDP0WpjWrYF67jt9UEwptNPMfoJdX+I8C0S6ZtGvpUtwjoEDgmCKBEHBMAQJAgAOAhACQcEwEAYCQOwVAgyidSAJTqbHXXlclrmkjgeKazzbemL9KAyazUzutbk+HsjOnuB7sgDd8l0EmnnhFAyk+AHzF7ptDVXSrliL+5XFKTy9kBpJLAHY8P1jlNwbbyzpdSweZps8vdNna0V/nVb8oDU5COQWKwFlIrk2OxJICttlN+XBlzQkkBSlhY9hZIswJIIMd4lE1uXQ7ldi3KLJeqCYKXmDXsSvevHsLU8WNe4AfpsTNsJelbmO5F1U81oI7BNhMsmSKfYmUIQB+pE6hAgP1A6mAgU3UyZYAgTJCBAhCEeyAza+51USw0mzzTeW22dDxbU+ZdKEXlLY5jAYCITIBw0NHdoVtsaKyFWPpSSjx3EmsfUi/TH35LOlyfAFWGatLqPIsTbfSZ5ZjldgdgPSRkpRUk8pkZj8Ku66ehveP9jYwhWVyLGJICpiNFjQjArK5FrW4k1uEquL9RLEB7Mee6Kiul7tAl6bEwReLEPagprFlFULHBYyWp9UE/golHcDvkIQghCECiQgQIEAQIggCASEIASnV2eVp5z9kXGDxibjo2vdgcCcnKTb5YmAkAC5CluThBW0Qpe462W4vcZRb2AujKMFnA3mOyPp9P0K+nMlHD2LsRxnHGwFMoyfLyLKPTjJa098diub2w/qBdobXXqItcPZndPOUvpmmeijvFP4CAxJFjEYFTQjLJCMCuSEmvSmWMWSzFgZ5DLeAJBhw0VlVPZlkvVBP3FsQa3mvHsFGl+lr2FktyV7WNe48luB2QEIRUIEgECAIECAIECAIBIQgEOT47J9MI525OucPx2X40I57AcsgEWOOFkKTlBW/0QBktgFXJdDOMRWW+SQrTbk9kkD8u+cZ7APutlu87sZKTf8AkFc+V0muiHVjb7ACFXUvbBjuj62ehq0qVTbW7RyNdT5cya1jGtlE7uil16WD9lg4TOv4XPqocfZlZa2Kx2KwiqQjLJFbAVirnA7E7gUTWGLB+ostWJFPDyVDTWwtL3aHnuVR9NiYBm+mSZdj4K7UPXLMEB2CEAQQJCBUCQgBIQgECQgBIQgBPPeNOT1jzwksHoTh+PLF1b94gc2uPUy6ccQBRHbI97xD6mbfrcnxl5YVx/YOMBUcmmcO28YX3JGPU1nsNXU5PHY2V6buyauKqoNt7bs7Gi0qUE5Lcq09MVFSwdGp7Gb03OVyhscfxmr05SO1DgxeKVdVTaIrzNiwdLwn+VLbuYLo4e50PCU1TP6m3KtrFYzFZUVyEZZIrYCsSQ7FkBXaspMzy5NUt4P4M0gh+YopnsyyG8RLCoeW8RIT6Vhsat5h9CuSeWFd8hCEEIQgUSEIASE7EAIQBAhCEAJyPH4/ypY90dc53jkc6WD9p/4CuXUsQRLY9Tihq16UM1vkx+uk8UzhiRZCrCyxox9WS9R2GrgUQXJrW8djPVs2jTBYrM2tSLYPETTSzHF52NVOxFbocCamHVU0NW8oaSysGmXmtfT0SyaPDY4031bLPF68B0SxpoG45dLWKxmKyskkVyRbIqkAjAxmK+AFxlNGaaNS5KLliTCVXW92iTQIvEh5FQlLWWiSjlix9NiLJLLCu0QhCCEIQKJCBAhCEAIQBAhCEAJi8Xj1aJ/EkzaUa6PVpZr7hXEisRQyC9gHOusPFbFkPkWCyOkZaBrE0aYv04M7XqRfHaP0CrKuTVDGDLBpRzyaKfVuBspfBd3Kql6UWx5LGaweKV9cNiihYogvg6l0FOuWfY50Y9MEvY3HPoGBjMVmmCSK5ItZXICsAWABHyJeuGWMWxZrAyS5LHuhJjQ/KVFctmW8rJXNDVv0AdoJAEUSEIAQgCBCEIAQgQQIEAQIJcuqma/0scS54pm/9LCuG+QEk1yLGeWc3VdW9y9bszrbg0VkrUDG5bysCrdPBIEVbBYWDXp4mWBtp2iCtUB1sZ1YoYLlNS4NMmsf4Uvoc7sbb5dNMvnYxs1y59EYrGYrNMFZXIsYkgKmAZisBWTmLQWBEGWaBX3Q9yxJlUHiRpDSRVlrYukUyW/IHfIQhFFEIQAhAQAkIQAoIAgQhCAEq1Lxp5/QtKtTvp7P9rCvPwbccMGHkkN4JjqUeG0Ybhq59maoPCyZJVP80HkeqxrZixqVrqe+B3zkz1SxMtm8RyZb1ZCe5r81RhycpzfYeM5zaXYrNrowk5yz2NdK6cvsUaOC6d+S3UWOMemPLGjPrdVmyuqG/rXU/bcZld9Sp0Upv8zlF5+5YzXLHZGBjMVmnMrK5FjEkBSwDSFYAF7jCvkgrvW+TM9pI2WrMPoZJIqHe5W1uWJ5ihWnko7ZCEIqBAFAEhCAEgAgEIAgQhCAQS7eif8AtY4sv5TT9miK8vFzsn0r7liojnDbyaKKkr7cfoz/AMi2RyS1qQv8NBrackxJK2p89SDOM/6ZP9w+rPwRoarss15cobcHOmuixNdzdFtaabzuot/8CrKosvjDbl+wK7dRJ/hwST9xaKX5fnNZ6s4HjKzqXTx3GJrbRLxFLMZV/uNLVayp9dtPUly0XaKLnLMs9PsdWutYeyw+xNVwtV4mtXXGmEenfL3Ol2ORPQeZ4pONeFXF9b+Fk67Nxz6KxWMxWVkrEkOxGBVIUeQgAFYwGAOYtGSa3NaM90cSYQlb2wMxIP1Y9xyjskIQioggCASACBAgCASEIASEIBBZRzkchBx6o9OqlF94yiUyRp1P4Ot6+ykn9u4NbUqbcLeMt0yV0jMkGS2CkCZhtlsT64/U3xX4LX6lgx4zd8I6lEOqvP8A6i1Iroin4ZXFpqUW/wC7KowxI3upwrth/SpdS+6/7TM3TuKRq0rxhHTjPEPscvTp5N3V0w5SeDK1l0q9V9neTS/z/ksYNOn/AA6k+Zycvt2GZ0jl0RisZis0yViMdisCqQhZIrfIAAwgYAXJXfHhlncW1ZgBkTxIsK5cliw1krNdkgCEaEICBBCAgUSEIAQgCASERACQhCDm+JxxbGXujJO5utQsy4rhrsdHxOOaYy9mc6JL8bn2FjJY2kmvqJZZFd8v2Rc6oNbxTKLUk0orBNjeVKVvl8nZ0MOqtr4ORWsM7nhkeqKJVhLLY02QlYm4NOE/oVXaaUUp1NWVviUWaPEqX5TaWy3OVXnOFKUfoy7/AKmX2N9LlFbxYznLVT8mHH9TX9KK6dOp465zl8ZOlTXCqtRhFRXwT4Zf0WlCtYWyWEvoZab6b4TnvBReHkujY7arel/knsc/VtV1pQ2Vk3Jm545deuhPTvo6oyUkUShJdhrb5aWqmnpz6N18sx1a2f8AESi8dEE2yovewjH0epd9dk74x8uK2wt8jyhVOHmVzwn2fYDLIrfJfZBxxlc8MokApGRkAVhe6wBhXAGSxYySD9PI96xIpzjYqO6TOCMwa/WeV+HBrq7/AAQbJXRj3RK74Wflkmeed023lsaq2Vc04vDCvSIhk0erVySe0jXkCBAEAkQAgEIAgQhCAVauHmaea74ycdHY1Fqrrfu9kceW02vkzWuT52KZL1ZLOrZiSMupoLc7nheyODCTydfQ29MecAjpazoensXujz0o9Fh17JSnhPgy6mj09S7D0nw2leyNs7FXRKb4imzBpewfF7vL0fQuZvH2Ifo+D2ebXdFvdrqH8qFs1XNd+pGLwKeNT0/qi1/wdCacJ9S4TOsZ/rM6NrKnG53S3io9X7HDi2qrrO8tv+z0t0Y2aWUXnpkjkuiquKgstJ53Dkri3Xoqq/1ephvzPytNHjmTXua6pJ1tOGV9CvDy3jAFkK9RdJVqCVS2XH7mbUV+VY4pqWO6Onoc4bTzjlFdukru65VvEsZx7sDlMgZJxbT5QoAYUQHcCu9ZWTLJbm2azBmSS3LEdbVXKmmUu/Y8/ZNzk5SeWzf4tc3KNae3JzWRYgcgIFX1WyrkpJ8He01yuqUlz3POI6XhNjVrrzs1kI64RQppgMQAQCQhAJkqsvUNluxb7cemJjun0pyfYCXXdc0m8v2Mk5fiuP3DFqyStXKWGZ9RJxvb9hWufWh8C9yQkpRz7iSynlGG11EeqxL5N0FjVdC49jm1WOM0/Y6FFvVNWLaWCWNR04wykxJtNSiJC+yUd2jO4uNrlFvDJRo08MZOV4td5t0kn6YbHRuv8jSzm9njb6nEeXXJvk1zE36t0Fjr1EJJ8M9G4KSZ5OM3F+k9bpsuqHWsScU39cGz+vUuYmnl1Uyrlu47GO9dMnhGqt9GpXtLZlWrhhsOLNXLGUNJ7FMc5+CybxEDd4XLM2vgSDlHVS6ez4F8Mk1a/lMlE86uX1CKNfp5QlKzmOcJmI62ujKWshXl9NkcY9jl2Qdc3CXKeGAoozFCmMs16nsaUVWL1gYtXZ5k+oqqj1yw+AN5LaFj7kWJdXGMFKP0ZSa5Rc6fuZpwcWJVqIu093k2xsXZlPYCTb6Vyyo2X+IW3NYfSl2RdotdKM1Gx5T7nOlFwk4vlchi8NBHqIvKygmXw+xyp6XzH+xpk0llsAtpLLM9t2eHsJddy28RRg1NrnWpwk+nhoC2zUxVii3u2ZZTk7pQm8p7C2LrgrV35Em+uKl3WzA06ePTGWecmXU/zpG1fkWecbmHUb3SDXPo6eePS/sX4yZIbSNUXlGbGpfqKPqNVClwk2VVpORu0zw0Z1uHXXhJouqjnktSTWTNrdQtPVs/W+ET0rB4vf12qmP5Y8/UztY07920VbzuWX8sssfVbCCfpOkYbfB/DVqrlZJ+iPK9zvQ3bMHgHplZF9jbGWLMfJXOksi+rC55RNT66lL3Re45kpIWda6Jpccgch5UsEm8osui1MpkBr8Oli5b8k0/89t/qK9G8XR+pa10aucV+tlRu1efTKOFLKwzneIx65eao4ktpL59zoa19LrfxuiWUVXVNt4lOOEBwADzi4ycWsNCsiomRpMCGQHKjU5S24NEIKKDXHYZ8mG8NGCUFEpvrytlualHH7AlHJBy+Mphi+maa7M1WafLyiuOnmmnjg3GVMm3Jt8sg86pp5aE7lR2PDrPTGT/AE4/Yu1F+YvCzhZwYtE2tOvrsJGyVV+Z7+4Dqx6iEoPaXKKK8tyqff8AuNYnTdmPGcoa2qUrY2VrnDAr0+fXW+Gv2Gqq8vPU00aOlRTaWG+REsyAaS9Jgv8A5svqdBrY5tm839Q3x6Edmi9voSl27lPS++xdmM6nHKzgRev9X1yTWUaqp/JzaW0t9i1Ta7sxYuutLUwqrzJ7+xytRbK6xzl/8Fcm3liWvEWxImq4yxZKXwXaePmRnZ7NYMqeK38nT0cMaSC/U8m2ddLw2XTf1R4a6mbqo+rq5zuc3w2ca3OVrUYRW7Ymr8St1EvL0sZKPuuWGXUv11GnfTOeZL+lHN1Hi1t04R06UV1brnJg/h5t5se/sdPw+qEcYisr4KLdRHEuDHYdHULKMF0MdiCad/iR3NuoWNZ1e6TOdW8TX1OjflumfvHH7FiNesWa65eyEqTu07hHlbotvWdLF/BToH39ngDB4jRODjZNY6tn9TEdTWeZdCcXu474OWyVQXIwvcIGdbIie4mcRbHitsmG13UlHJX1vOQNiNvJqRLVvVkmcMrQ2dioOzKLdP1STjzktLa1y/gIr1GKq4xjtjA/l/xVUZwx1LZlHmSlOSlutzdoklQsAIqNoKaTcQs0S3m0VTSwyopfDEjsyxlb4Ipm/Q38GOqHVmb+xtl+VlMYqOyCy4quhul8GeUemWGbbFlRK9TCPk9XdYBKrhZOuWMp/XcdyUnmKx8FNMn5sc7743OlLTVRuUEtpIOnz9YUyu97YLtXBVWuEc4XuZpbtExOvAfaJ2aYxjCPU8RhE49SzdHPudN+u6qt/le7XuVg7jLUcJxqz+51dHp4V0+iP3Jq4RhGMYpJLZGjSrFGSsuZqIdNrZbo5LqRNWsy+pVptp7Abr+EZ7q8wbNM94Jlcv5LYHNSxLJ1I4npK3+mWDmWbSOnpd9FLPZpiFarpf8Ahsp8O3618Flm+kf2E8N4mwjQ1VC5+neaOHrqPIvaX5XvH6Han6tO2+YvYxeIRU9N1PmL2/YVY5IUwBRFf//Z\"";
		 * Mat mat=ImageUtil.base642Mat(msg); ImageUtil.Mat2Img(mat, ".jpeg",
		 * "E:\\1.jpeg");
		 * 
		 * Mat image =ImageUtil.base642Mat(msg); Mat check=ImageUtil.getFace(image);
		 * 
		 * ImageUtil.Mat2Img(check, ".jpeg", "E:\\2.jpeg");
		 */
	    
	    //String checkSucess=ImageUtil.Mat2BufImg(check, ".png").toString();
		String path="E:\\opencv4.4.0\\age-and-gender-classification\\img\\huge\\";
		/*
		File imgFile = new File(path);
		File listFile[] = imgFile.listFiles();
		for (File f : listFile) {
			if (f.getName().endsWith(".jpeg")) {
				Mat test = ImageUtil.imagePath2Mat(f.getPath());
				Mat check = ImageUtil.getFace(test);
				ImageUtil.Mat2Img(check, ".jpeg", path + UUID.randomUUID()+".jpeg");
			}
		}
		 */
		 Mat _des=ImageUtil.imagePath2Mat("E:\\opencv4.4.0\\age-and-gender-classification\\img\\008.jpg");
		 //Mat check=ImageUtil.getFace(_des);
		 ImageUtil.compareHist(_des);
		
	}
	
	/*
	static{
			// 在使用OpenCV前必须加载Core.NATIVE_LIBRARY_NAME类,否则会报错
			logger.info("load OPENCV...");
	        logger.info("library:"+System.getProperty("java.library.path"));
			//System.loadLibrary("opencv_java412");
			//System.load("E:/opencv/opencv//build/java/x64/opencv_java412.dll");
			logger.info(Core.NATIVE_LIBRARY_NAME);
			System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
			
			FACEBOOK = new CascadeClassifier(FACEPATH);
			PROFILEFACEFACEBOOK = new CascadeClassifier(PROFILEFACEPATH);
			 
			AGENET = Dnn.readNetFromCaffe(AGE_TEXT, AGE_MODEL);
			
			GENDER=Dnn.readNetFromCaffe(GENDER_TEXT, GENDER_MODEL);
			
			try {
				initComPare("E:\\opencv4.4.0\\age-and-gender-classification\\img\\test_me");
			} catch (IOException e) {
				e.printStackTrace();
			}
	}*/

	/**
	 * 将jar包的文件复制到能读取的地方
	 * @param src
	 * @param target
	 * @throws IOException
	 */
	public void copyFile(String src,String target) throws IOException {
		ClassPathResource classPathResource = new ClassPathResource(src);
		InputStream inputStream =classPathResource.getInputStream();
		File docxFile = new File(target);
        // 使用common-io的工具类即可转换
        FileUtils.copyInputStreamToFile(inputStream,docxFile);
        inputStream.close();
	}
	
	@Override
	public void run(String... args) throws Exception {
		// 在使用OpenCV前必须加载Core.NATIVE_LIBRARY_NAME类,否则会报错
					logger.info("load OPENCV...");
			        logger.info("library:"+System.getProperty("java.library.path"));
					//System.loadLibrary("opencv_java412");
					//System.load("E:/opencv/opencv//build/java/x64/opencv_java412.dll");
					logger.info(Core.NATIVE_LIBRARY_NAME);
					System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
					logger.info("加载opencv文件路径:"+PATH);
				
			        copyFile("/opencv/"+FACEPATH,PATH+FACEPATH);
			        copyFile("/opencv/"+PROFILEFACEPATH,PATH+PROFILEFACEPATH);
			        copyFile("/opencv/"+AGE_TEXT,PATH+AGE_TEXT);
			        copyFile("/opencv/"+AGE_MODEL,PATH+AGE_MODEL);
			        copyFile("/opencv/"+GENDER_TEXT,PATH+GENDER_TEXT);
			        copyFile("/opencv/"+GENDER_MODEL,PATH+GENDER_MODEL);
			        
			        
					FACEBOOK = new CascadeClassifier(PATH+FACEPATH);
					PROFILEFACEFACEBOOK = new CascadeClassifier(PATH+PROFILEFACEPATH);
					 
					AGENET = Dnn.readNetFromCaffe(PATH+AGE_TEXT, PATH+AGE_MODEL);
					
					GENDER=Dnn.readNetFromCaffe(PATH+GENDER_TEXT, PATH+GENDER_MODEL);
					
					try {
						srcMap.clear();
						initComPare(SCAN_PATH);
					} catch (IOException e) {
						e.printStackTrace();
					}
	}
}