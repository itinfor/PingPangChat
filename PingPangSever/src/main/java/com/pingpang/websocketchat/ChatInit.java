package com.pingpang.websocketchat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.pingpang.websocketchat.send.ChatSend;

@Component
public class ChatInit implements CommandLineRunner {

	//日志操作
    private Logger logger = LoggerFactory.getLogger(ChatInit.class);
    
    @Value("${netty.port}")
    private String nettyPort;
    
    @Value("${opencv.save.path}")
    private String opencvSave;
    
    @Override
    public void run(String... args) throws Exception {
    	
    	ChatSend.PATH=opencvSave;
    	logger.info("图片保存地址:{}",opencvSave);
        logger.info("The netty start to initialize ...");
        new WebsocketChatServer(Integer.valueOf(nettyPort)).run();
    }
}

