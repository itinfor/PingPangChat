package com.pingpang.websocketchat;

import java.io.Serializable;
import java.util.Objects;


public class ChartUser implements Serializable{

	private static final long serialVersionUID = -6320343402886493808L;

	//用户ID
	private String id;
	
	//用户代号
	private String userCode;
	
	//用户名称
	private String userName;
	
	//用户邮箱
	private String userEmail;
	
	//用户电话
	private String userPhone;
	
    //用户头像路径	
	private String userImagePath;
	
	//用户密码
	//@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private String userPassword;
	
	//用户性别 0:男,1:女'
	private String userSex;
	
	//用户状态-1:注销,0:离线,1:在线,2:禁言
	private String userStatus;
	
	//用户创建日期 
	private String userCreateDate;
	
	//当前用户的ip
	private String ip;
	
	//直播方式 0->werbrtc 1->flv
	private String liveType;
	
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getUserImagePath() {
		return userImagePath;
	}

	public void setUserImagePath(String userImagePath) {
		this.userImagePath = userImagePath;
	}

	public String getUserSex() {
		return userSex;
	}

	public void setUserSex(String userSex) {
		this.userSex = userSex;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public String getUserCreateDate() {
		return userCreateDate;
	}

	public void setUserCreateDate(String userCreateDate) {
		this.userCreateDate = userCreateDate;
	}
	
	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getLiveType() {
		return liveType;
	}

	public void setLiveType(String liveType) {
		this.liveType = liveType;
	}


	@Override
	public String toString() {
		return "ChartUser [id=" + id + ", userCode=" + userCode + ", userName=" + userName + ", userEmail=" + userEmail
				+ ", userPhone=" + userPhone + ", userImagePath=" + userImagePath + ", userPassword=" + userPassword
				+ ", userSex=" + userSex + ", userStatus=" + userStatus + ", userCreateDate=" + userCreateDate + ", ip="
				+ ip + ", liveType=" + liveType + "]";
	}

	@Override
	public boolean equals(Object obj) {
		ChartUser cu=(ChartUser)obj;
		if(null!=cu && (this==cu || this.getUserCode().equals(cu.getUserCode()) || this.getId().equals(cu.getId()))) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.userCode);
	}
	
    
	
}
