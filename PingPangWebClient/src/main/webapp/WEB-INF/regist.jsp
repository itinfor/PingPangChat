<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <title></title> 
  <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/font.css">
  <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/login.css">
  <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/xadmin.css">
  
  <script src="${httpServletRequest.getContextPath()}/jquery.min.js"></script>
  <script src="${httpServletRequest.getContextPath()}/layer.js"></script>
  <script src="${httpServletRequest.getContextPath()}/layui.js"></script> 
</head>
<body class="login-bg">

<div class="login layui-anim layui-anim-up">
        <div class="message">PingPangChat</div>
        <div id="darkbannerwrap"></div>
   <form class="layui-form" action="${httpServletRequest.getContextPath()}/userController/addUser" method="POST">
   
    <input name="userName" placeholder="昵称"  type="text" autocomplete="off"  lay-verify="required|userName" class="layui-input" >
            <hr class="hr15">
    <input name="userCode" placeholder="编码"  type="text" autocomplete="off" lay-verify="required|userCode" class="layui-input">
            <hr class="hr15">
    <input name="userPassword" placeholder="密码"  type="text" autocomplete="off" lay-verify="required|userPassWord" class="layui-input">
            <hr class="hr15">
    <button class="layui-btn" lay-filter="regist"  lay-submit="" style="width:100%;">注册</button>
    <hr class="hr15">
  </form>
    <button class="layui-btn"  style="width:100%;" onclick="location.href='${httpServletRequest.getContextPath()}/user/index'" >登录</button>
</div> 
  <script>
             layui.use(['form','layer','laydate' ], function() {
					var form = layui.form
					   ,layer = layui.layer
					   ,layedit = layui.layedit
					   ,laydate = layui.laydate;
					 
					//自定义验证规则
					form.verify({
						userName : function(value) {
							if (value.length < 2 || value.length >8) {
								return '代号至少需要2-8位字符';
							}
						},
						userCode : [ /^[0-9A-Za-z]{2,6}$/, '字母和数字2-8位' ],
						userPassWord : [ /^[0-9A-Za-z]{2,6}$/, '字母和数字2-8位' ]
					});

					//监听提交
					form.on('submit(regist)', function(data) {
						/*layer.alert(JSON.stringify(data.field), {
						  title: '最终的提交信息'
						})*/
						return true;
					});

					var errorMsg = "${errorMsg}";
					if (null != errorMsg && "" != errorMsg) {
						layer.alert(errorMsg), {
							title : '错误提示'
						}
					}
				});
		</script>
</body>
</html>