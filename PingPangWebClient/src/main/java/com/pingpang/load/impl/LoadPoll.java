package com.pingpang.load.impl;

import com.pingpang.load.LoadInterface;
import com.pingpang.redis.RedisPre;

/**
 * 轮询处理 从最低点开始
 * 
 * @author dell
 *
 */
public class LoadPoll extends LoadInterface {

	@Override
	public String getAddress() {
		return this.redisService.getServer(RedisPre.SERVER_ADDRES_ZSET, 1);
	}
}
